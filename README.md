conformal mappings from right half plane to the ... ( n-petal flower)  

change n and run batch file using Maxima CAS.  

For n the program makes svg file an.svg




see figure 3.3 page 20 from: [Root finding methods: a dynamical approach by Olea Martínez, Javier](http://diposit.ub.edu/dspace/handle/2445/65849)
>>>
Figure 3.3: Re(z) = 50, Re(z) = 100 and Re(z) = 150 (left) and their images for all the branches of the function $`\frac{1}{\sqrt[n]{z}}`$ with n = 3 (right).
>>>




# periods from 1 to 10 

![a1.png](a1.png)  


![a2.png](a2.png)  

![a3.png](a3.png)  

![a4.png](a4.png)  

![a5.png](a5.png)  

![a6.png](a6.png)  

![a7.png](a7.png)  

![a8.png](a8.png)  

![a9.png](a9.png)  

![a10.png](a10.png)  


## period 1


Compare with:
* [Conformal mapping from right half plane to unit circle](https://commons.wikimedia.org/wiki/File:Conformal_mapping_from_right_half_plane_to_unit_circle.svg)


## period 3

[M 597 LECTURE NOTES TOPICS IN MATHEMATICS COMPLEX DYNAMICS by LUKAS GEYER](http://www.math.montana.edu/geyer/2016-fall-m597/materials/s2016-complex-dynamics-notes.pdf) see page 40 Figure 10


# See also:


MALGORZATA STAWISKA in [LIOUVILLE THEOREM WITH PARAMETERS: ASYMPTOTICS OF CERTAIN RATIONAL INTEGRALS IN DIFFERENTIAL FIELDS](https://arxiv.org/abs/1004.5536)
>>>
... approximate Fatou coordinates for analytic maps $`f`$ in a neighborhood of an $`f0(z) = z + z^{q+1} + ...`$ with $`q > 1`$.
These are coordinates in which f looks like a translation. The first step in constructing Fatou coordinate for $`f_0`$ consists in lifting $`f_0`$ to a neighborhood of infinity by the coordinate change $`z \to \frac{−1}{qz^q}`$.
We considered f belonging to an one-parameter family of polynomials $`P_λ(z) = λz + z^2`$ with $`λ_0 = e^{ 2πip/q}`$ and $`λ = e^{2πi(p/q+u)}`$ , with p, q coprime integers and u in a sufficiently small neighborhood of 0 in $`C`$.

>>>

# notation of exponentiation

[Fractional Exponents also called "Radicals" or "Rational Exponents"](https://www.mathsisfun.com/algebra/exponent-fractional.html)
* A fractional exponent like 1/n means to take the n-th root
* nth degree root of number x   
* x raised to the [fractional exponent](https://medium.com/i-math/what-do-fractional-exponents-mean-1bb9bd2fa9a8)
 
$` x^ \frac {1}{n} = \sqrt [n] {x} `$


[Negative Exponents](https://www.mathsisfun.com/algebra/negative-exponents.html)
* Dividing is the inverse (opposite) of multiplying
* Reciprocal of Positive Exponent


$` x^{-n} = \frac{1}{x^n} `$

# License

This project is licensed under the   Apache License Version 2.0, January 2004 - see the [LICENSE](LICENSE) file for details


# git

## commands
```
cd existing_folder
  git init
  git remote add origin git@gitlab.com:adammajewski/fatou-coordinate-maxima-cas.git
  git add .
  git commit
  git push -u origin master
```

## markdown format
[GitLab](https://gitlab.com/) uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)




